module.exports = {
    env: {
        test: {
            sourceType: 'module',
            presets: [
                'react-app',
                [
                    '@babel/preset-react',
                    {
                        runtime: 'automatic',
                    },
                ],
            ],
            // Replace `import.meta` by `process`
            plugins: [
                function () {
                    return {
                        visitor: {
                            MetaProperty(path) {
                                path.replaceWithSourceString('process');
                            },
                        },
                    };
                },
            ],
        }
    },
};
