import '@testing-library/jest-dom';

import { configure } from '@testing-library/dom';

// Update process.env here, this includes values that you should find in import.meta, as it's translated into process by babel
process.env.MY_DATA = 'DATA';

configure({
    testIdAttribute: 'test-id',
});
