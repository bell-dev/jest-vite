import {isStringEmpty} from 'helpers/string';

it("Helper `isStringEmpty`", () => {
    expect(isStringEmpty()).toEqual(true);
    expect(isStringEmpty(undefined)).toEqual(true);
    expect(isStringEmpty(null)).toEqual(true);
    expect(isStringEmpty('')).toEqual(true);
    expect(isStringEmpty('string')).toEqual(false);
})
