export const isStringEmpty = (string) => {
    return !Boolean(string);
};
