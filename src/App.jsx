import { SuperComponent } from 'SuperComponent/SuperComponent';

import './App.scss';

function App() {
    return (
        <>
            <SuperComponent content="hello world" />
            <SuperComponent content="hello galaxy" />
        </>
    );
}

export default App;
