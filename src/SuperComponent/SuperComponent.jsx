import { isStringEmpty } from 'helpers/string';

import styles from './SuperComponent.module.scss';

export const SuperComponent = ({ content }) => {
    if (isStringEmpty(content)) {
        return null;
    }

    return (
        <p className={styles.wonderfulContent} test-id="content">
            {content}
        </p>
    );
};
