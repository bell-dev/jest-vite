import { render, screen } from '@testing-library/react';
import { SuperComponent } from 'SuperComponent/SuperComponent';

describe('SuperComponent', () => {
    it('empty ontent', () => {
        const { queryByTestId } = render(<SuperComponent />);
        expect(queryByTestId('content')).toBeNull();
    });

    it('content `hello world`', () => {
        render(<SuperComponent content="hello world" />);
        expect(screen.getByTestId('content')).toBeInTheDocument();
        expect(screen.getByTestId('content')).toBeVisible();
        expect(screen.getByTestId('content')).toHaveTextContent(/^hello world$/);
    });

    it('content `hello galaxy`', () => {
        render(<SuperComponent content="hello galaxy" />);
        expect(screen.getByTestId('content')).toBeInTheDocument();
        expect(screen.getByTestId('content')).toBeVisible();
        expect(screen.getByTestId('content')).toHaveTextContent(/^hello galaxy$/);
    });
});
