Jest-vite boilerplate

**This boilerplate includes**:

-   Rewrite of `import.meta` into `process`
-   Custom test id attribute, conigurable in `.jest/jest.setup.js`
-   Auto ignore of cypress default test location (`cypress/`)
-   Includes commandes to run test with watch mode, or return coverage
-   Includes @testing-library/react to allow to test react component as well as regular JS functions
-   Compatible with import aliases

You ill find 2 example of testing, one of a regular JS function, in `src/helpers`, and one of a react component, in `src/SuperComponent`

Oh, and by the way, don't be afraid by the CSS 😉

Fork of [getting-started-with-react-using-vitejs](https://pranshushah.tech/getting-started-with-react-using-vitejs), simplified in some points, and fixing some issues.
