import { resolve } from 'path';

import reactRefresh from '@vitejs/plugin-react-refresh';
import { defineConfig } from 'vite';

import { getDirectories } from './plugins/utils/directory';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
    return {
        esbuild: {
            jsxInject: "import React from 'react'",
        },
        resolve: {
            alias: {
                ...getDirectories(resolve(__dirname, 'src/')).reduce(
                    (aliases, folder) => ({ ...aliases, [folder]: resolve(__dirname, `src/${folder}`) }),
                    {}
                ),
            },
        },
        plugins: [
            reactRefresh()
        ],
    };
});
